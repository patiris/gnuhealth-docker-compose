#!/bin/bash
set -e

export TRYTOND_DATABASE_URI
export TRYTOND_CONFIG
export TRYTOND_DATA

if [ "$1" = 'trytond' ]; then
	# Listen on all interfaces
	#sed -i "/^#listen = \[::\]:8000/s/^#//" "$TRYTOND_CONFIG"
	
	# Run setup scripts for the server
	if [ -d /docker-entrypoint-init.d ]; then
		for f in /docker-entrypoint-init.d/*.sh; do
			[ -f "$f" ] && . "$f"
		done
	fi
	
	# Reset permissions to the data directory
	sudo chown -R gnuhealth "$TRYTOND_DATA"
	
	trytond -c $TRYTOND_CONFIG
fi

exec "$@"
